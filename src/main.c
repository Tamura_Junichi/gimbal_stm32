/**
  ******************************************************************************
  * @file      gimbal_filter.c
  * @brief     STM32F4 calc setting file.
  * @author    Junichi
  * @date      27-October-2017
  * @version   1.0
  ******************************************************************************
  */ 
  

#include"gimbal_stm32.h"

int main()
{


/**
  * @brief     Initialization and Configuration
  *
@verbatim   
 ===============================================================================
                 ##### Initialization and Configuration #####
 ===============================================================================  
@endverbatim
  *
  */
  
  uint8_t res;
  setup_serial();
  data_init();
  filter_init();
  ctr_init();
  tim_init();
  delay(2000000);
  setup_i2c_for_mpu6050();
  delay(2000000);
  setup_mpu6050();
  
  res = read_mpu6050_reg(MPU6050_REG_WHOAMI);
  serial_output_hexbyte(USART3, res);
  res = read_mpu6050_reg(MPU6050_REG_PWR_MGMT_1);
  serial_output_hexbyte(USART3, res);


/**
  * @brief     It is a function processed in the main loop
  *
@verbatim   
 ===============================================================================
                        ##### LOOP PROCESSING #####
 ===============================================================================  
@endverbatim
  *
  */
  while (1)
  {
    data_processing(tim2_get_time());
  }

  return 0;
}


#ifdef USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line){
  while(1);
}
#endif
